cmake_minimum_required(VERSION 3.11 FATAL_ERROR)

project(mse VERSION 0.1.0 DESCRIPTION "My super experiment")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/modules" CACHE STRING "Modules for CMake" FORCE)

include_directories("${PROJECT_BINARY_DIR}")

# Setting libdir to (lib or lib64)
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(CMAKE_INSTALL_LIBDIR lib64)
endif()

find_package(ROOT REQUIRED)
include(ROOTMacros)

add_subdirectory(Event)
add_subdirectory(Analysis)
add_subdirectory(src)

add_custom_target(uninstall "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake")
