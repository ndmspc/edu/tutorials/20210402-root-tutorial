#!/bin/bash

PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"
for d in bin lib lib64 include share; do
  [ -d $PROJECT_DIR/$d ] && rm -rf $PROJECT_DIR/$d
done
echo "Cleaning is done"
