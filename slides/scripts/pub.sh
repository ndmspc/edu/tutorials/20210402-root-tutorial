#!/bin/bash
REVEALJS_NAME="reveal.js"
REVEALJS_VERSION=${REVEALJS_VERSION-"4.1.0"}
REVEALJS="$REVEALJS_NAME-$REVEALJS_VERSION"
PRESENTATION_DIRS="src"

function LinkRevealJS() {
  local revealjs=$1
  local indexdir=$2
  rp=$(realpath --relative-to=$indexdir $revealjs)
  for l in css js dist plugin;do
    ln -sfn $rp/$l
  done
}

function GenerateRedirect() {
cat <<EOF > $2/index.html
<!DOCTYPE html>
<html>
<head>
  <title>Slides redirect</title>
  <meta http-equiv="refresh" content="0; URL=$1" />
</head>
<body>
  <p>Loading ...</p>
</body>
</html>
EOF

}

rm -rf public
mkdir public
cd public
curl -sL https://github.com/hakimel/reveal.js/archive/$REVEALJS_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }

PUB_DIR=$(pwd)
for d in $(find ../$PRESENTATION_DIRS -mindepth 1 -maxdepth 1 -type d);do
  cp -rf $d $PUB_DIR
done
for p in $(find $PUB_DIR -name index.html -not -path "*reveal.js*" );do
  pd=$(dirname $p)
  cd $pd
  LinkRevealJS $PUB_DIR/$REVEALJS .
  cd -
  GenerateRedirect $(basename $pd) $(pwd)
done
cd -

