#!/bin/bash
REVEALJS_NAME="reveal.js"
REVEALJS_VERSION=${REVEALJS_VERSION-"4.1.0"}
REVEALJS="$REVEALJS_NAME-$REVEALJS_VERSION"
PRESENTATION_DIRS="src"

function LinkRevealJS() {
  local revealjs=$1
  local indexdir=$2
  rp=$(realpath --relative-to=$indexdir $revealjs)
  for l in css js lib plugin;do
    ln -sfn $rp/$l
  done
}

if [ ! -d $REVEALJS ];then
	echo "Downloaging '$REVEALJS' ..."
	curl -sL https://github.com/hakimel/reveal.js/archive/$REVEALJS_VERSION.tar.gz | tar xz
  cd $REVEALJS
  npm install || { echo "Error running 'npm install' in $REVEALJS directory !!!"; exit 1; }
  cd -
fi

if [ -n "$1" ];then
	[ -d $PRESENTATION_DIRS/$1 ] || { echo "Directory '$PRESENTATION_DIRS/$1' doesn't exists !!!"; exit 2; }
	echo "Switching to $PRESENTATION_DIRS/$1"
	rm -f $REVEALJS/index.html
	for d in $(find $PRESENTATION_DIRS/$1 -mindepth 1 -maxdepth 1);do
		ln -sfn ../$d $REVEALJS/$(basename $d)
	done

	echo ""
	echo "You are now set for presentation '$PRESENTATION_DIRS/$1' in '$REVEALJS' directory"
	echo ""
	echo "Now you have to do:"
	echo ""
	echo "  cd $REVEALJS"
	echo "  npm start"
	echo ""
	echo "Edit 'index.html' in '$REVEALJS' directory"
	echo ""
else
	for p in $(find $PRESENTATION_DIRS -name index.html -not -path "*reveal.js*" );do
		p=${p//src\//}
		p=${p//\/index.html/}
		echo "  $p"
	done
fi
