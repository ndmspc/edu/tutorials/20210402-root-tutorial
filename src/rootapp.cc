#include <TApplication.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TH2S.h>

int main(int argc, char ** argv)
{
    TApplication theApp("App", &argc, argv);

    auto mainCanvas = new TCanvas("Main", "Main window", 0, 0, 600, 600);
    auto mainHistorgam = new TH2D("n", "Highlight", 10, -5, 5, 10, -5, 5);
    mainHistorgam->SetStats(0);
    mainHistorgam->Draw("colz");
    mainCanvas->Update();
    mainHistorgam->SetHighlight();

    theApp.Run();

    return 0;
}
